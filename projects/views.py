from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm

# Create your views here.


@login_required
def list_projects(request):
    context = {"list_projects": Project.objects.filter(owner=request.user)}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    show_project = Project.objects.get(id=id)
    context = {"show_project": show_project}
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save
            list_projects = form.save(commit=False)
            list_projects.save()
            return redirect("/projects/")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
